# Drupal CI/CD

#HSLIDE

@snap[north ubuntu-heavy]
## Moi
@snapend

@snap[west ubuntu]
Pierre Paul Lefebvre
<br/>
FIXRS
@snapend

#HSLIDE

@snap[north ubuntu-heavy span-90]
## C'est quoi le CI/CD ?
@snapend


#HSLIDE

@snap[north ubuntu-heavy span-90]
### Erreurs communes
@snapend

@ol
- Je n'ai pas de tests
- On travaille encore avec FTP
- Je n'utilise pas Docker
- Je suis seul à travailler sur le projet
@olend

#HSLIDE

@snap[north ubuntu-heavy span-90]
### Solutions
@snapend

- CircleCI
- Bitbucket Pipelines
- Cloud Build (Google)
- Jenkins
- Bamboo (Atlassian)
- Gitlab-CI

#HSLIDE

@snap[north ubuntu-heavy span-90]
### Gitlab-CI
@snapend

@ul
- Multi-platforme (utile pour compiler une application iOS ou linux par exemple)
- Multi-langage (en autant que votre runner permet de rouler du Go)
- Parallèle
- En temps réel
- Versionné
- Cache
@ulend

#HSLIDE

### Types de runner

@ul
- SSH
- Docker 
- Kubernetes
@ulend

#HSLIDE

@snap[north]
### Lingo
@snapend

@ul[west span-100]
- shared : partagé entre projets
- locked : spécifique à un projet
- tags : roule seulement si le projet a un tag
- protected branch : roule seulement les protected branch
@ulend


#HSLIDE

### Artifacts

Sauvegarde le resultat d'un build.

- /dist
- /build

#HSLIDE

@snap[north]
### Mais après?
@snapend

@ul[west]
- Dokku
- Flynn
- Kubernetes
@ulend

#HSLIDE

### Twelve factor - D7

```
$url = parse_url(getenv('DATABASE_URL'));

$databases = array (
  'default' =>
  array (
    'default' =>
    array (
        'driver' => 'mysql',
        'database' => substr(urldecode($url['path']), 1),
        'username' => urldecode($url['user']),
        'password' => isset($url['pass']) ? urldecode($url['pass']) : '',
        'host' => urldecode($url['host']),
        'port' => isset($url['port']) ? urldecode($url['port']) : '',
        'prefix' => 'd7_',
    ),
  ),
);
```

#HSLIDE

### .gitlab-ci.yml

```yaml
deploy_prod:
    only:
      - master
    script: 
      - rsync -az --progress . ubuntu@<server-name>:~/drupal --exclude .gitignore --exclude .git --exclude .gitlab-ci.yml --exclude Dockerfile --exclude docker-compose.yml --exclude tmp
deploy_dev:
    only:
      - develop 
    script: 
      - git push dokku@localhost:$PROJECT_NAME $CI_COMMIT_SHA:refs/heads/master
```

#HSLIDE

![Gitlab-ci example](assets/img/gitlab.png)

#HSLIDE

# Start simple

#HSLIDE

### Merci!

À lire : [https://slides.com/jamescandan/continuous-delivery-7](https://slides.com/jamescandan/continuous-delivery-7)
